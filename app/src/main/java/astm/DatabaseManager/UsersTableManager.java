package astm.DatabaseManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astm.Authentication.CurrentUser;

public class UsersTableManager
{
    private DatabaseManager dbManager = new DatabaseManager();
    private static final String USERS_TABLE = "Users";

    public UsersTableManager()
    {}

    public Boolean addEntries(List<User> entries)
    {
        boolean result = true;
        for (User user : entries)
        {
            result = dbManager.addEntries(USERS_TABLE, composeQueryFromUser(user));
        }

        return result;
    }

    public List<User> getEntries(HashMap<String, String> searchCriteria)
    {
        List<User> result = new ArrayList<User>();

        HashMap<String, List<String>> queryResult = dbManager.getEntriesByFields(USERS_TABLE, searchCriteria, new ArrayList<String>());

        result = fetchUsersFromQuery(queryResult);

        return result;
    }

    public User getEntry(int primaryKey)
    {
        HashMap<String, List<String>> queryResult = dbManager.getEntriesByPrimaryKey(USERS_TABLE, primaryKey);
        List<User> fetchedUsers = fetchUsersFromQuery(queryResult);

        return fetchedUsers.get(0);
    }

    public boolean updateEntry(User updatedUser)
    {
        boolean result = true;
        int primaryKey = 0;

        primaryKey = updatedUser.getID();
        HashMap<String, String> queryResult = composeQueryFromUser(updatedUser);
        queryResult.put("userId", Integer.toString(primaryKey));
        result = dbManager.updateEntries(USERS_TABLE, queryResult, primaryKey);

        return result;
    }

    public void deleteEntry(int primaryKey)
    {

    }

    private List<User> fetchUsersFromQuery(HashMap<String, List<String>> queryResult)
    {
        List<User> result = new ArrayList<User>();
        if (queryResult.size() == 0)
            return result;

        int numberOfUsers = queryResult.get("emailAddress").size();

        for (int i = 0; i < numberOfUsers; i++)
        {
            int id = Integer.parseInt(queryResult.get("userId").get(i));
            String email = queryResult.get("emailAddress").get(i);
            String password = queryResult.get("password").get(i);
            String firstName = queryResult.get("firstName").get(i);
            String lastName = queryResult.get("lastName").get(i);
            String phoneNumber = queryResult.get("phoneNumber").get(i);
            String cnp = queryResult.get("cnp").get(i);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date startDate = new Date();
            Date endDate = new Date();
            try
            {
                startDate = dateFormat.parse(queryResult.get("startDate").get(i));
                endDate = dateFormat.parse(queryResult.get("endDate").get(i));
            }
            catch (java.text.ParseException ex)
            {
                ex.printStackTrace();
            }

            User fetchedUser = new UserBuilder()
                    .setID(id)
                    .setEmail(email)
                    .setPassword(password)
                    .setFirstName(firstName)
                    .setLastName(lastName)
                    .setStartDate(startDate)
                    .setEndDate(endDate)
                    .setPhoneNumber(phoneNumber)
                    .setCnp(cnp)
                    .createUser();

            result.add(fetchedUser);
        }

        return result;
    }

        private HashMap<String, String> composeQueryFromUser(User user)
        {
            HashMap<String, String> result = new HashMap<String, String>();
            result.put("emailAddress", user.getEmail());
            result.put("password", user.getPassword());
            result.put("firstName", user.getFirstName());
            result.put("lastName", user.getLastName());
            result.put("phoneNumber", user.getPhoneNumber());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = dateFormat.format(user.getStartDate());
            result.put("startDate", date);
            date = dateFormat.format(user.getEndDate());
            result.put("endDate", date);
            result.put("cnp", user.getCnp());

            return result;
        }
}
