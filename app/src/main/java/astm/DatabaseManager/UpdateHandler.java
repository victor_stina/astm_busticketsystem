package astm.DatabaseManager;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import astm.Authentication.CurrentUser;
import astm.busticketsystem.LoginActivity;
import astm.busticketsystem.PassengerActivity;
import astm.busticketsystem.PurchaseActivity;

public class UpdateHandler extends AsyncTask<String, Void, Boolean>
{
    private Context mContext;

    public UpdateHandler(Context context)
    {
        mContext = context.getApplicationContext();
    }
    @Override
    protected Boolean doInBackground(String... fields)
    {
        final String id = fields[0];
        final String email = fields[1];
        final String password = fields[2];
        final String firstName = fields[3];
        final String lastName = fields[4];
        final String phoneNumber = fields[5];
        final String cnp = fields[6];
        final String startDate = fields[7];
        final String endDate = fields[8];

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date startingDate = new Date();
        Date endingDate = new Date();

        try
        {
            startingDate = dateFormat.parse(startDate);
            endingDate = dateFormat.parse(endDate);
        }
        catch (java.text.ParseException ex)
        {
            ex.printStackTrace();
        }

        int userId = Integer.parseInt(id);

        UsersTableManager userManager = new UsersTableManager();

        User updatedUser = new UserBuilder()
                .setID(userId)
                .setEmail(email)
                .setPassword(password)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setPhoneNumber(phoneNumber)
                .setCnp(cnp)
                .setStartDate(startingDate)
                .setEndDate(endingDate)
                .createUser();

        CurrentUser loggedUser = CurrentUser.getInstance();
        loggedUser.setUser(updatedUser);

        boolean result = userManager.updateEntry(updatedUser);

        return result;
    }

    @Override
    protected void onPostExecute(Boolean isValidated)
    {
        super.onPostExecute(isValidated);
        if (isValidated)
        {
            mContext.startActivity(new Intent(mContext, PassengerActivity.class));
        }
        else
        {
            Toast.makeText(mContext, "Error", Toast.LENGTH_LONG).show();
        }
    }
}
