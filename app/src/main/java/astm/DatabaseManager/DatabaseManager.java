package astm.DatabaseManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import astm.RequestHandler.RequestHandler;

public class DatabaseManager
{
    private static final String API_URL = "https://busticketrest.azurewebsites.net/api/";
    private static final String PASSENGERS_TABLE = "Passengers";
    private static final String STAFF_TABLE = "Staff";

    private RequestHandler requestHandler;

    public DatabaseManager()
    {
        requestHandler = new RequestHandler();
    }

    public boolean addEntries(String tableName, HashMap<String, String> entryFields)
    {
        boolean result = true;

        String response = requestHandler.sendPostRequest(API_URL + tableName, entryFields);
        try
        {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("error"))
                result = false;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public HashMap<String, List<String>> getEntriesByFields(String tableName, HashMap<String, String> searchCriteria, List<String> desiredValues)
    {
        HashMap<String, List<String>> queryResult = new HashMap<String, List<String>>();
        String response = requestHandler.sendGetRequest(API_URL + tableName);
        try
        {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = (JSONObject)jsonArray.get(i);
                Iterator<String> keysItr = jsonObject.keys();
                HashMap<String, List<String>> helperMap = new HashMap<String, List<String>>();
                int matchedCriteriaCounter = 0;

                while(keysItr.hasNext())
                {
                    String key = keysItr.next();
                    String value = String.valueOf(jsonObject.get(key));

                    if ((!desiredValues.isEmpty() && !desiredValues.contains(key)))
                        continue;;

                    if (!helperMap.containsKey(key))
                        helperMap.put(key, new ArrayList<String>());

                    helperMap.get(key).add(value);

                    if (!searchCriteria.containsKey(key) || value == null ||  searchCriteria.get(key) == null || !(searchCriteria.get(key).equals(value)))
                        continue;

                    matchedCriteriaCounter++;
                }

                if (matchedCriteriaCounter == searchCriteria.size())
                {
                    for (Map.Entry<String, List<String>> entry : helperMap.entrySet() )
                    {
                        if (!queryResult.containsKey(entry.getKey()))
                            queryResult.put(entry.getKey(), new ArrayList<String>());

                        queryResult.get(entry.getKey()).addAll(entry.getValue());
                    }
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return queryResult;
    }

    public HashMap<String, List<String>> getEntriesByPrimaryKey(String tableName, int primaryKey)
    {
        HashMap<String, List<String>> queryResult = new HashMap<String, List<String>>();
        String response = requestHandler.sendGetRequest(API_URL + tableName + "/" + Integer.toString(primaryKey));
        try
        {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = (JSONObject)jsonArray.get(i);
                Iterator<String> keysItr = jsonObject.keys();

                while(keysItr.hasNext())
                {
                    String key = keysItr.next();
                    String value = String.valueOf(jsonObject.get(key));

                    if (!queryResult.containsKey(key))
                        queryResult.put(key, new ArrayList<String>());

                    queryResult.get(key).add(value);
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return queryResult;
    }

    public boolean updateEntries(String tableName, HashMap<String, String> fieldsToUpdate, int primaryKey)
    {
        boolean result = true;

        String response = requestHandler.sendPutRequest(API_URL + tableName + "/" + Integer.toString(primaryKey) , fieldsToUpdate);
        try
        {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("error"))
                result = false;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteEntries(String tableName, HashMap<String, String> searchCriteria)
    {
        boolean result = true;
        return result;
    }

}
