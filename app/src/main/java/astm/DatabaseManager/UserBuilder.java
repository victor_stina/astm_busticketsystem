package astm.DatabaseManager;

import java.util.Date;

public class UserBuilder {
    private int id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String cnp;
    private Date startDate;
    private Date endDate;

    public UserBuilder setID(int id) {
        this.id = id;
        return this;
    }

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserBuilder setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public UserBuilder setCnp(String cnp) {
        this.cnp = cnp;
        return this;
    }

    public UserBuilder setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public UserBuilder setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public User createUser() {
        return new User(id, email, password, firstName, lastName, phoneNumber, cnp, startDate, endDate);
    }
}