package astm.busticketsystem;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import astm.Authentication.RegisterHandler;

public class RegisterActivity extends Activity
{
    private Button loginButton;
    private Button registerButton;

    private View.OnClickListener registerClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            EditText emailText = findViewById(R.id.emailText);
            EditText passwordText = findViewById(R.id.passwordText);
            EditText cnfPasswordText = findViewById(R.id.passwordConfirmText);
            EditText firstNameText = findViewById(R.id.firstnameText);
            EditText lastNameText = findViewById(R.id.lastnameText);
            EditText phoneNumberText = findViewById(R.id.phoneNumberText);
            processRegisterRequest(emailText.getText().toString(), passwordText.getText().toString(),
                            cnfPasswordText.getText().toString(), firstNameText.getText().toString(),
                            lastNameText.getText().toString(), phoneNumberText.getText().toString());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_view);

        registerButton = findViewById(R.id.registerBtn);
        registerButton.setOnClickListener(registerClickListener);
    }

    private boolean processRegisterRequest(String email, String password, String cnfPassword, String firstName, String lastName, String phoneNumber)
    {

        if (email.equals("") || password.equals("") || cnfPassword.equals(""))
        {
            Toast.makeText(this, "Please enter your email address and password", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!password.equals(cnfPassword))
        {
            Toast.makeText(this, "Passwords do not match", Toast.LENGTH_LONG).show();
            return false;
        }

        new RegisterHandler(this).execute(email, password, firstName, lastName, phoneNumber, "1900-01-01 00:00:00", "1900-01-01 00:00:00");

        return true;
    }
}
