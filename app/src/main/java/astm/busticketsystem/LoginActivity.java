package astm.busticketsystem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import astm.Authentication.CurrentUser;
import astm.Authentication.LoginHandler;
import astm.FaceDetection.FaceDetectionActivity;

public class LoginActivity extends AppCompatActivity
{
    static {
        System.loadLibrary("native-lib");
    }

    private Button loginButton;
    private Button registerButton;

    private View.OnClickListener loginClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            EditText emailText = findViewById(R.id.emailText);
            EditText passwordText = findViewById(R.id.passwordText);

            processLoginRequest(emailText.getText().toString(), passwordText.getText().toString());
        }
    };

    private View.OnClickListener registerClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switchToSignUp();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        loginButton = findViewById(R.id.loginBtn);
        registerButton = findViewById(R.id.registerBtn);
        loginButton.setOnClickListener(loginClickListener);
        registerButton.setOnClickListener(registerClickListener);
    }

    private boolean processLoginRequest(final String email, final String password)
    {
        if (email.equals("") || password.equals(""))
        {
            Toast.makeText(this, "Please enter your email address and password", Toast.LENGTH_LONG).show();
            return false;
        }

        new LoginHandler(this.getApplicationContext()).execute(email, password);
        return true;
    }

    private void switchToSignUp()
    {
        this.startActivity(new Intent(this.getApplicationContext(), RegisterActivity.class));
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
