package astm.busticketsystem;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import astm.Authentication.CurrentUser;
import astm.Authentication.LoginHandler;
import astm.DatabaseManager.UpdateHandler;
import astm.DatabaseManager.User;
import astm.DatabaseManager.UsersTableManager;
import astm.Notification.AlarmReceiver;
import astm.Notification.DeviceBootReceiver;
import astm.Notification.NotificationHelper;


public class PurchaseActivity extends AppCompatActivity
{
    private Spinner subTypeSpinner;
    private Button buySubButton;
    private DatePicker datePicker;
    private TextView priceText;
    private TimePicker timePicker;
    private List<String> subList;
    private double price;
    private String subType;
    private Calendar c;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchase_subscription_view);

        buySubButton = findViewById(R.id.buyBtn);
        buySubButton.setOnClickListener(buySubClickListener);
        subTypeSpinner = findViewById(R.id.subTypeSpinner);
        priceText = findViewById(R.id.totalPriceText);
        datePicker = findViewById(R.id.datePicker);
        timePicker = findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        addSubscriptionTypesOnSpinner();
        displaySubscriptionPrice();
    }

    private View.OnClickListener buySubClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            updateUserSubscriptionDates();
            setNotification();
        }
    };

    private void updateUserSubscriptionDates()
    {
        CurrentUser loggedUser = CurrentUser.getInstance();
        Date startDate = getDateFromDateTimePicker();
        Date endDate = calculateEndDate();
        String UserId = Integer.toString(loggedUser.getUserID());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String newStartDate = dateFormat.format(startDate);
        String newEndDate = dateFormat.format(endDate);

        processPutRequest(UserId, loggedUser.getUserEmail(), loggedUser.getUserPassword(), loggedUser.getUserFirstName(), loggedUser.getUserLastName(),
                loggedUser.getUserPhoneNumber(), loggedUser.getUserCNP(), newStartDate, newEndDate);
    }

    private boolean processPutRequest(String id, String email, String password, String firstName, String lastName, String phoneNumber, String cnp, String startDate, String endDate)
    {
        new UpdateHandler(this.getApplicationContext()).execute(id, email, password, firstName, lastName, phoneNumber, cnp, startDate, endDate);
        return true;
    }

    private Date calculateEndDate()
    {
        Date startDate = getDateFromDateTimePicker();
        Date endDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);

        if (subType.equals(subList.get(0)))
        {
            c.add(Calendar.HOUR, 1);
            endDate = c.getTime();
        }
        else if (subType.equals(subList.get(1)))
        {
            c.add(Calendar.DAY_OF_WEEK, 1);
            endDate = c.getTime();
        }
        else
        {
            c.add(Calendar.MONTH, 1);
            endDate = c.getTime();
        }

        return endDate;
    }

    public Date getDateFromDateTimePicker()
    {
        int year, month, day;
        int hour, minute;

        day = datePicker.getDayOfMonth();
        month = datePicker.getMonth() + 1;
        year = datePicker.getYear();
        hour = timePicker.getHour();
        minute = timePicker.getMinute();

        String date = Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(day) +
                " " + Integer.toString(hour) + ":" + Integer.toString(minute) + ":00";

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = new Date();
        try
        {
            startDate = dateFormat.parse(date);
        }
        catch (java.text.ParseException ex)
        {
            ex.printStackTrace();
        }

        return startDate;
    }

    public void addSubscriptionTypesOnSpinner()
    {
        subList = new ArrayList<>();
        subList.add("One hour ticket");
        subList.add("One day ticket");
        subList.add("Monthly subscription");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, subList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subTypeSpinner.setAdapter(spinnerArrayAdapter);
    }

    public void displaySubscriptionPrice()
    {
        final double[] priceList = {3.5, 7.8, 50};

        subTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                for (int i = 0; i < subList.size(); i++)
                {
                    if (parent.getItemAtPosition(position).toString().equals(subList.get(i)))
                    {
                        price = priceList[i];
                        priceText.setText(price + " RON");
                        subType = subList.get(i);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }

    public void setNotification()
    {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = this.getPackageManager();
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Date endDate = calculateEndDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.setTime(endDate);

        if (manager != null)
        {
            manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

        pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
    }
}
