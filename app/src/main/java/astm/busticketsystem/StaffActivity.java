package astm.busticketsystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import astm.FaceDetection.FaceDetectionActivity;

public class StaffActivity extends AppCompatActivity
{
    private Button faceScanButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_staff_interface);

        faceScanButton = findViewById(R.id.scanBtn);
        faceScanButton.setOnClickListener(faceScanClickListener);
    }

    private View.OnClickListener faceScanClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switchToFaceScan();
        }
    };

    private void switchToFaceScan()
    {
        this.startActivity(new Intent(this.getApplicationContext(), FaceDetectionActivity.class));
    }
}
