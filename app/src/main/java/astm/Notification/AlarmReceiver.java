package astm.Notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class AlarmReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        NotificationHelper notification = new NotificationHelper(context);
        notification.createNotification("Your subscription has expired!", "Tap to buy another subscription");
    }
}