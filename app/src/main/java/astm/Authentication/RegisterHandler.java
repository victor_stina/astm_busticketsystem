package astm.Authentication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import astm.DatabaseManager.DatabaseManager;
import astm.DatabaseManager.User;
import astm.DatabaseManager.UserBuilder;
import astm.DatabaseManager.UsersTableManager;
import astm.busticketsystem.LoginActivity;

public class RegisterHandler extends AsyncTask<String, Void, Boolean>
{
    private Context mContext;

    public RegisterHandler(Context context)
    {
        mContext = context.getApplicationContext();
    }

    @Override
    protected Boolean doInBackground(String... fields)
    {
        final String email = fields[0];
        final String password = fields[1];
        final String firstName = fields[2];
        final String lastName = fields[3];
        final String phoneNumber = fields[4];
        final String startDate = fields[5];
        final String endDate = fields[6];
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startingDate = new Date();
        Date endingDate = new Date();
        try
        {
            startingDate = dateFormat.parse(startDate);
            endingDate = dateFormat.parse(endDate);
        }
        catch (java.text.ParseException ex)
        {
            ex.printStackTrace();
        }

        UsersTableManager userManager = new UsersTableManager();

        User newUser = new UserBuilder()
                .setEmail(email)
                .setPassword(password)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setPhoneNumber(phoneNumber)
                .setStartDate(startingDate)
                .setEndDate(endingDate)
                .createUser();

        Boolean result = userManager.addEntries(Arrays.asList(newUser));

        return result;
    }

    @Override
    protected void onPostExecute(Boolean isValidated)
    {
        super.onPostExecute(isValidated);
        if (isValidated)
        {
            mContext.startActivity(new Intent(mContext, LoginActivity.class));
        }
        else
        {
            Toast.makeText(mContext, "Error", Toast.LENGTH_LONG).show();
        }
    }
}
