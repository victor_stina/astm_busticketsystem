package astm.Authentication;

import java.util.Date;

import astm.DatabaseManager.User;

public class CurrentUser
{
    private static CurrentUser currentUser = new CurrentUser();
    private User user;

    private CurrentUser()
    { }

    public static CurrentUser getInstance()
    {
        return currentUser;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public int getUserID()
    {
        return user.getID();
    }

    public String getUserEmail()
    {
        return user.getEmail();
    }

    public String getUserPassword()
    {
        return user.getPassword();
    }

    public String getUserFirstName()
    {
        return user.getFirstName();
    }

    public String getUserLastName()
    {
        return user.getLastName();
    }

    public Date getUserStartDate()
    {
        return user.getStartDate();
    }

    public Date getUserEndDate()
    {
        return user.getEndDate();
    }

    public String getUserPhoneNumber() { return user.getPhoneNumber(); }

    public String getUserCNP() { return user.getCnp(); }
}
