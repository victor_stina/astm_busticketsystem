package astm.Authentication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import astm.DatabaseManager.DatabaseManager;
import astm.DatabaseManager.User;
import astm.DatabaseManager.UsersTableManager;
import astm.busticketsystem.PassengerActivity;
import astm.busticketsystem.StaffActivity;

public class LoginHandler extends AsyncTask<String, Void, Boolean>
{
    private Context mContext;

    public LoginHandler(Context context)
    {
        mContext = context.getApplicationContext();
    }

    @Override
    protected Boolean doInBackground(String... fields)
    {
        final String email = fields[0];
        final String password = fields[1];

        List<User> fetchedUsers = new ArrayList<User>();

        UsersTableManager userManager = new UsersTableManager();

        fetchedUsers = userManager.getEntries(new HashMap<String, String>() {{put("emailAddress", email); put("password", password); }});

        if (fetchedUsers.size() == 1)
        {
            CurrentUser loggedUser = CurrentUser.getInstance();
            loggedUser.setUser(fetchedUsers.get(0));
            return true;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean isValidated)
    {
        super.onPostExecute(isValidated);
        if (isValidated)
        {
            if(CurrentUser.getInstance().getUserEmail().endsWith("stb.com"))
            {
                mContext.startActivity(new Intent(mContext, StaffActivity.class));
            }
            else
            {
                mContext.startActivity(new Intent(mContext, PassengerActivity.class));
           }

        }
        else
        {
            Toast.makeText(mContext, "Your email or password is incorrect", Toast.LENGTH_LONG).show();
        }
    }
}
